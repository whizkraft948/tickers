
import { WebSocketServer } from 'ws';

export function Webserver(server) {


    const wss = new WebSocketServer({ server });

    wss.getUniqueID = function () {

        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
        }
        return Date.now() + '-' + s4();
    };

    wss.on('connection', (ws, req) => {

        // const ip = req.socket.remoteAddress;
        // console.log(ip);

        ws.id = wss.getUniqueID();

        // ws.id =  Date.now();
        //connection is up, let's add a simple event
        ws.on('message', (message) => {

            console.log('Message', message.toString());

            // Log the received message and send it back to the client
            // console.log('received: %s', message);
            // return ws.send(message.toString());
            return wsMessage(ws, 1, 'Subscribed Successfully', { c_id: ws.id, message: message.toString() });
        });

        ws.on('close', () => {
            console.log('Connection closed By => ' + ws.id);
        })

        //send immediatly a feedback to the incoming connection    
        return wsMessage(ws, 1, 'Connected Successfully', { c_id: ws.id })
    });

    const wsMessage = (ws, status, msg, data = null) => {
        let err = {
            status_code: status.toString(),
            msg: msg
        };
        if (data != null) {
            Object.assign(err, data);
        }
        err = JSON.stringify(err);
        return ws.send(err);
    }

}