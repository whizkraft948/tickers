import express from 'express';
import https from 'https';
import fs from 'fs';
import bodyParser from 'body-parser';
import cors from 'cors';

const app = express();

app.get('/', (req, res) => {
    res.send("IT'S WORKING!")
})

const corsOptions = {
    origin: 'https://localhost:5000'
};
// Add cors origin :
app.use(cors(corsOptions));

// Add Body Parser for content-type : application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

const httpsOptions = {
    key: fs.readFileSync('./security/cert.key'),
    cert: fs.readFileSync('./security/cert.pem')
};

var server = https.createServer(httpsOptions, app);

import { Webserver } from './socketServer.js'
Webserver(server);

server.listen(5000, () => {
    console.log('server is listen 5000');
});

